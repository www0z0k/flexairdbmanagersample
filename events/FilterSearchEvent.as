/**
 * Created by www0z0k on 11/03/15.
 */
package events {
import flash.events.Event;

    public class FilterSearchEvent extends Event{
        public static const FILTER_SEARCH_EVENT:String = 'filter search event';
        public static const SELECT:String = 'mode select';
        public static const EDIT:String = 'mode edit';
        public static const CREATE:String = 'mode create';
        public static const KILL:String = 'mode kill';

        private function getQueryFirstPart():String{
            var res:String = '';
            switch(_mode){
                case SELECT:
                    res = 'select * from employees ';
                    break;
                case EDIT:
                    res = 'update employees set DeptID = "' + _targetDept + '"';
                    break;
                case KILL:
                    res = 'delete from employees ';
                    break;
            }
            return res;
        }

        private var _firstName:String;
        private var _lastName:String;
        private var _department:String;
        private var _position:String;
        private var _uid:String;
        private var _targetDept:String;

        private var _mode:String;
        public function FilterSearchEvent(uid:String = '', first:String = '', last:String = '', dept:String = '', pos:String = '', mode:String = SELECT, targetDept:String = '') {
            _firstName = first;
            _lastName = last;
            _department = dept;
            _position = pos;
            _uid = uid;
            _mode = mode;
            _targetDept = targetDept;
            super(FILTER_SEARCH_EVENT, true);
        }
        public function get query():String{
            if(_mode == CREATE){
                return 'insert into employees (DeptID, FirstName, LastName, Position) values ("' + _department + '", "' + _firstName + '", "' + _lastName + '", "' + _position + '")';
            }else {
                var res:String = getQueryFirstPart();
                var args:Array = new Array();
                if (_department) {
                    args.push("DeptID = " + _department);
                }
                if (_uid) {
                    args.push("EmplID = " + _uid);
                }
                if (_position) {
                    args.push("Position = '" + _position + "'");
                }
                if (_firstName) {
                    args.push("FirstName = '" + _firstName + "'");
                }
                if (_lastName) {
                    args.push("LastName = '" + _lastName + "'");
                }
                res += args.length ? (' WHERE ' + args.join(' AND ')) : '';
                return res;
            }
        }

        public function cloneWithDifferentMode(newMode:String):FilterSearchEvent{
            return new FilterSearchEvent(_uid, _firstName, _lastName, _department, _position, newMode, _targetDept);
        }

        public function cloneWithDifferentTarget(newTarget:String):FilterSearchEvent{
            return new FilterSearchEvent(_uid, _firstName, _lastName, _department, _position, _mode, newTarget);
        }

        public function get mode():String {
            return _mode;
        }
    }
}
