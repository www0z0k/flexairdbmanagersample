/**
 * Created by www0z0k on 11/03/15.
 */
package events {
    import flash.events.Event;

    public class DeptEvent extends Event{
        public static const DEPT_EVENT:String = 'dept event';
        public static const KILL_DEPT:String = 'mode kill';
        public static const EDIT_DEPT:String = 'mode edit';
        public static const CREATE_DEPT:String = 'mode create';
        public static const SELECT_DEPT:String = 'mode select';
        private var _id:int;
        private var _mode:String;
        private var _name:String;
        public function DeptEvent(id:int, mode:String = SELECT_DEPT, name:String = '') {
            _id = id;
            _mode = mode;
            _name = name;
            super(DEPT_EVENT);
        }

        public function get id():int {
            return _id;
        }

        public function set id(value:int):void {
            _id = value;
        }

        public function get mode():String {
            return _mode;
        }

        public function set mode(value:String):void {
            _mode = value;
        }

        public function get name():String {
            return _name;
        }

        public function set name(value:String):void {
            _name = value;
        }
    }
}
